# simple bing wallpaper downloader inspired by
# the bingwallpaper bash script by Whizzzkid (me@nishantarora.in) / Lioh (lioh@gmx.ch)

import std/[times, os, osproc, posix, strutils]
import std/httpclient
import std/re
import std/json

# Base URL.
let bing = "http://www.bing.com"

# API end point.
let api = "/HPImageArchive.aspx?"

# Response Format (json|xml).
let format = "&format=js"

# For day (0=current; 1=yesterday... so on).
let day = "&idx=0"

# Market for image.
let market = "&mkt=en-US"

# API Constant (fetch how many).
let constante = "&n=1"

# Image extension.
let extn = ".jpg"

# Size.
let size = "1920x1080"

# Date.
# let today=$(date +"%Y%m%d")
let dt = parse(getDateStr(), "yyyy-MM-dd")
let today = dt.format("yyyyMMdd")


# Collection Path.
let path = "$HOME/Bilder/Bing/"
  
let fullpath = bing & api & format & day & constante & market

# create a http client and
# get url 
var client = newHttpClient()
let erg = client.getContent(fullpath)

# parse response as json
let jsonNode = parseJson(erg)

# echo jsonNode["images"][0]["urlbase"]
let mystring = jsonNode["images"][0]["urlbase"].getStr()

echo mystring

var matches: array[2, string]
if match(mystring, re"\/th\?id\=?(\w+.*)", matches):
  # echo matches[0]
  let filename = today & "_" & matches[0] & extn
  echo filename
  

